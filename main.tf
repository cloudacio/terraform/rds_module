locals {
  db_parameter_family = {
    "mariadb"  = "mariadb10.4"
    "mysql"    = "mysql8.0"
    "postgres" = "postgres12"
  }
  subnet_groups = {
    "false" = "${var.environment}-private"
    "true"  = "${var.environment}-public"
  }
}

resource "aws_db_instance" "default" {
  allocated_storage            = var.allocated_storage
  apply_immediately            = "true"
  auto_minor_version_upgrade   = "false"
  backup_retention_period      = var.backup_retention_period
  backup_window                = var.backup_window
  db_subnet_group_name         = local.subnet_groups[var.publicly_accessible]
  deletion_protection          = var.deletion_protection
  engine                       = var.engine
  engine_version               = var.engine_version
  identifier                   = var.db_identifier
  instance_class               = var.instance_class
  maintenance_window           = var.maintenance_window
  multi_az                     = var.multi_az
  name                         = var.database_name
  parameter_group_name         = join("", aws_db_parameter_group.default.*.name)
  password                     = var.database_password
  performance_insights_enabled = var.performance_insights_enabled
  port                         = var.database_port
  publicly_accessible          = var.publicly_accessible
  skip_final_snapshot          = "true"
  storage_type                 = "gp2"
  username                     = var.database_user
  vpc_security_group_ids       = [var.security_groups]

  tags = {
    environment = var.environment
  }
}

resource "aws_db_parameter_group" "default" {
  name        = var.db_identifier
  description = var.db_identifier
  family      = local.db_parameter_family[var.engine]
}

