# Module for creation of RDS instance

### Supported Engines and Version

```
mysql 8.X
mariadb 10.X
postgres 12.X
```

### Prerequisites

- Subnet Groups: There needs to be 2 subnet groups for RDS for each environment: one using only private subnets (Non Internet Face) called `${var.environment}-private` and 
one using only public subnets (Internet Face) called `${var.environment}-public`


### Usage
```
module "rds_production" {
  source                  = "git::ssh://git@gitlab.com/cloudacio/terraform/rds_module.git?ref=v<tag>"
  allocated_storage       = 30
  backup_retention_period = 0
  environment             = "production"
  backup_window           = "01:38-02:08"
  database_name           = "rds_db_name"
  database_password       = <VAR_PASSWD>
  database_port           = 3306
  database_user           = <VAR_USER>
  db_identifier           = "rds-instancename-production"
  deletion_protection     = "false"
  engine                  = "mysql"
  engine_version          = "8.0"
  instance_class          = "db.t3.small"
  maintenance_window      = "sat:10:58-sat:11:28"
  multi_az                = "false"
  publicly_accessible     = true
}
```
### Outputs

will look like this:
```
instance_address = "demo-production.chi0gebqxj0p.us-east-1.rds.amazonaws.com"
instance_endpoint = "demo-production.chi0gebqxj0p.us-east-1.rds.amazonaws.com:3306"
instance_id = "demo-production"
parameter_group_id = "demo-production"
```